-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2020 at 09:15 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `misaonline`
--

-- --------------------------------------------------------

--
-- Table structure for table `checkin`
--

CREATE TABLE `checkin` (
  `checkin_id` int(11) NOT NULL,
  `jadwal_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `telepon` varchar(16) NOT NULL,
  `alamat` text NOT NULL,
  `gender` enum('M','F') NOT NULL,
  `usia_id` int(11) NOT NULL,
  `asal_paroki` enum('DALAM','LUAR') NOT NULL,
  `paroki_lainnya` varchar(160) NOT NULL,
  `asal_lingkungan` int(11) NOT NULL DEFAULT 0,
  `asal_keuskupan` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `token` text NOT NULL,
  `code` text NOT NULL,
  `status` enum('CHECKED_IN','ABSENT','BLOCKED','ACTIVE') NOT NULL COMMENT '''CHECKED_IN => Sudah Masuk Gereja'',''ABSENT => Tidak Hadir'',''BLOCKED => Blacklist'',''ACTIVE => Baru Daftar Saja, Belum Checkin''',
  `ip_address` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE `configs` (
  `nama` varchar(160) NOT NULL,
  `kapasitas` int(11) NOT NULL,
  `passkey` varchar(50) NOT NULL,
  `image` varchar(160) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`nama`, `kapasitas`, `passkey`, `image`) VALUES
('Gereja Katedral St. Petrus Bandung', 200, '123', '');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `jadwal_id` int(11) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `grouping` enum('HARIAN','MINGGUAN') NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`jadwal_id`, `nama`, `tanggal`, `status`, `grouping`, `date_added`) VALUES
(1, 'Sabtu, 11 Juli 2020 - 17.00', '2020-07-01', 1, 'MINGGUAN', '2020-07-07 07:27:54'),
(2, 'Minggu, 12 Juli 2020 - 06.00', '2020-07-12', 1, 'MINGGUAN', '2020-07-07 07:27:54'),
(3, 'Minggu, 12 Juli 2020 - 17.00', '2020-07-12', 1, 'MINGGUAN', '2020-07-07 07:28:07'),
(4, 'Sabtu, 18 Juli 2020 - 17.00', '2020-07-18', 1, 'MINGGUAN', '2020-07-07 16:32:15'),
(5, 'Jumat, 10 Juli 2020 Pk. 08.00', '2020-07-10', 1, 'HARIAN', '2020-07-10 05:11:41');

-- --------------------------------------------------------

--
-- Table structure for table `lingkungan`
--

CREATE TABLE `lingkungan` (
  `lingkungan_id` tinyint(4) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lingkungan`
--

INSERT INTO `lingkungan` (`lingkungan_id`, `nama`, `status`, `date_added`) VALUES
(1, 'Lingkungan Kerubim', 1, '2020-07-07 07:09:51'),
(2, 'Lingkungan Mikael', 1, '2020-07-07 07:09:51'),
(3, 'Lingkungan Rafael', 1, '2020-07-07 07:10:05'),
(4, 'Lingkungan Serafim', 1, '2020-07-07 07:10:05'),
(5, 'Lingkungan Borromeus', 1, '2020-07-07 07:10:14'),
(6, 'Lingkungan Carolus', 1, '2020-07-07 07:10:14'),
(7, 'Lingkungan Gabriel', 1, '2020-07-07 07:10:25'),
(8, 'Lingkungan Gerardus', 1, '2020-07-07 07:10:25'),
(9, 'Lingkungan Antonius', 1, '2020-07-07 07:10:38'),
(10, 'Lingkungan Ignatius', 1, '2020-07-07 07:10:38'),
(11, 'Lingkungan Theresia', 1, '2020-07-07 07:10:49'),
(12, 'Lingkungan Yosep', 1, '2020-07-07 07:10:49'),
(13, 'Lingkungan Agustinus', 1, '2020-07-07 07:10:59'),
(14, 'Lingkungan Elisabeth', 1, '2020-07-07 07:10:59'),
(15, 'Lingkungan Maria', 1, '2020-07-07 07:11:10'),
(16, 'Lingkungan Kristoforus', 1, '2020-07-07 07:11:10'),
(17, 'Lingkungan Martinus', 1, '2020-07-07 07:11:20'),
(18, 'Lingkungan Monica', 1, '2020-07-07 07:11:20'),
(19, 'Lingkungan Stefanus', 1, '2020-07-07 07:11:23');

-- --------------------------------------------------------

--
-- Table structure for table `usia`
--

CREATE TABLE `usia` (
  `usia_id` int(11) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usia`
--

INSERT INTO `usia` (`usia_id`, `nama`, `status`, `date_added`) VALUES
(1, '13-17 tahun', 1, '2020-07-07 08:52:47'),
(2, '18-30 tahun', 1, '2020-07-07 08:52:47'),
(3, '31-50 tahun', 1, '2020-07-07 08:53:03'),
(4, '51-60 tahun', 1, '2020-07-07 08:53:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `checkin`
--
ALTER TABLE `checkin`
  ADD PRIMARY KEY (`checkin_id`);

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD UNIQUE KEY `nama` (`nama`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`jadwal_id`);

--
-- Indexes for table `lingkungan`
--
ALTER TABLE `lingkungan`
  ADD PRIMARY KEY (`lingkungan_id`);

--
-- Indexes for table `usia`
--
ALTER TABLE `usia`
  ADD PRIMARY KEY (`usia_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `checkin`
--
ALTER TABLE `checkin`
  MODIFY `checkin_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `jadwal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `lingkungan`
--
ALTER TABLE `lingkungan`
  MODIFY `lingkungan_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `usia`
--
ALTER TABLE `usia`
  MODIFY `usia_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
  
ALTER TABLE `checkin` ADD `cancel_code` VARCHAR(6) NULL DEFAULT NULL AFTER `code`;

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
