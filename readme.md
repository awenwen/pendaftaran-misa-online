Halo,

* Sistem ini merupakan freeware open source, basicnya dibuat oleh Byxel.net.
* Apabila ada penambahan code atau fitur silahkan sertakan comment dan berikan nama Anda sebagai penanda dan bukti contributor
* Aplikasi ini tidak untuk dijual
* Silahkan share aplikasi ini untuk keperluan Gereja
* Mohon untuk tidak menghilangkan copyright pada sistem ini
* Engine utama sistem ini merupakan Bootstrap, Native PHP 7+, dan MySQL 10+
* Seluruh data yang digunakan merupakan tanggung jawab pemakai Aplikasi

Langkah instalasi:

* Server mendukung Apache dan MySQL (PHP 7)
* Import database misaonline.sql
* Setup koneksi database dan nama server pada config.php
* Masuk phpmyadmin atau admin DB apapun (Heidi, Workbench, etc.)
* Pada table configs silahkan update data dengan nama Gereja dan kapasitas yang diinginkan, serta set pass-key untuk otorisasi, dan image header apabila diperlukan. Image di upload di root server.
** Sedikit saran: pass-key ini harus sering diubah untuk meningkatkan keamanan
* Pasang cron job run php file untuk menghapus qrcode temporary dan data umat yang tidak hadir (cron_file.php)
** Cron job disarankan setiap malam hari

`0	0	*	*	*	curl http://URL_WEB_ANDA/cron_file.php`

Langkah Penggunaan:

* User visit website
* Isi form
* User mendapat QRCode yang bisa di download
* User menunjukan QRCode pada petugas
* Petugas scan QRCode bisa dengan 2 cara:
** Melalui HP (bisa install dulu aplikasi QR Reader pada HP apabila kamera bawaan belum support baca QR)
** Melalui laptop dan 2D Barcode Reader, scan QR Code pada browser (Chrome disarankan)
* Petugas input pass-key sesuai arahan
* Sistem akan melakukan validasi QR Code user

Apabila ada pertanyaan lainnya bisa menghubungi di Byxel.net

2020-07-08