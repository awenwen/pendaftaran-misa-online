<?php
session_start();
//load librari QR Code
require("phpqrcode/qrlib.php"); //credit to https://github.com/t0k4rt/phpqrcode
require("config.php");
$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
if (mysqli_connect_errno()) {
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
	header("Location: https://www.google.com/");
}
//end koneksi database

//get data
$query = mysqli_query($conn, "SELECT c.*, j.nama as jadwal FROM checkin c LEFT JOIN jadwal j ON c.jadwal_id = j.jadwal_id WHERE code = '" . $_GET['x'] . "'");
$data = mysqli_fetch_assoc($query);
$nama = $data['nama'];
$telepon = $data['telepon'];
$jadwal = $data['jadwal'];
$cancelcode = $data['cancel_code'];
//end get data
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Eldwen - Byxel.net">
        <title>QR Code Pendaftaran Misa</title>
        <!-- Bootstrap core CSS -->
        <link href="assets/dist/css/bootstrap.css" rel="stylesheet">		
        <!-- Custom styles for this template -->
        <link href="form-validation.css" rel="stylesheet">
    </head>
    <body class="bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center">
                    <?php
                    if (/*isset($_SESSION['token']) && */isset($_GET['x'])) {
                        //echo $_GET['x'];
                        //ga di design ya, mepet
                        //generate file name
                        $rand = substr(md5(uniqid(rand(), true)), 0, 3);
                        $file = date('YmdHis', time());
						$jname = str_replace(",","",$jadwal);
						$jname = str_replace(".","",$jname);
						$jname = str_replace(" ","-",$jname);
                        $newfilename = $jname . '-' . $file . '-' . $cancelcode . '.png';

                        //create qrcode
                        QRcode::png(WEB_SERVER."qreader.php?token=".$_GET['x'], 'tmpqr/' . $newfilename, "H", 10, 5);
						echo '<div id="capture">';
                        echo '<center>';
                        echo "<img src='tmpqr/$newfilename'/ style='max-width:100%;'><br /><br />";
						echo 'Nama: '.$nama;
						echo '<br /> ';
						echo 'No. HP: '.$telepon;
						echo '<br />';
						echo 'Jadwal: '.$jadwal;
						echo '<br />';
						echo '<h5>Kode Pembatalan: '.$cancelcode.'</h5>';
						echo '<br /><br />';
						echo '</div>';
                        echo "<a href='tmpqr/$newfilename' download class='btn btn-primary btn-lg'>Download QR Code</a>";
						
                        echo '<br /><br /><h4>Download atau screenshot dan tunjukan QR Code ini pada petugas yang menjaga di Gereja untuk bisa mengikuti misa. Mohon untuk tidak terlambat, gerbang Gereja ditutup saat misa dimulai.</h4>';
                        echo '</center>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>
<?php
session_unset();
session_destroy();
?>