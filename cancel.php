<?php
session_start();
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Form pembatalan misa">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="author" content="Eldwen - Byxel.net">
        <meta name="generator" content="Jekyll v4.0.1">
        <title>Pembatalan Misa</title>

        <!-- Bootstrap core CSS -->
        <link href="assets/dist/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="form-validation.css" rel="stylesheet">
        <?php
        //koneksi database
        include('config.php');
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
            header("Location: https://www.google.com/");
        }
        //end koneksi database
        //generate token
        $token = bin2hex(random_bytes(64));
        $_SESSION['token'] = $token;
        //end generate token
        //get config for name and capacity
        $getConfig = mysqli_query($conn, "SELECT * FROM configs");
        $config = mysqli_fetch_assoc($getConfig);

        //end get config for name and capacity
        //create function to clean any post data
        function cleanstr($str) {
            $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
            $result = strtoupper($str);
            $result = htmlentities($result);
            $result = mysqli_real_escape_string($conn, $result);
            return $result;
        }

        //end
        //first initiate for data
        $code = '';
        //end initiate data
        //start form submited
        if (isset($_POST['submit']) && isset($_SESSION['token'])) {
            $code = $_POST['kode'];
			
            //double check if js not running
            if (!empty($code)) {
                //cek terdaftar atau tidak
                $cekRegistrasi = mysqli_query($conn, "SELECT * FROM checkin WHERE cancel_code = '" . $code . "'");
                
                if (mysqli_num_rows($cekRegistrasi) > 0) {
					//Kode pembatalan ditemukan
                    $sql = "DELETE FROM checkin WHERE cancel_code = '" . $code . "'";
					mysqli_query($conn, $sql);
					echo '<script>alert("Pembatalan Berhasil Dilakukan. Terima Kasih");</script>';
					echo "<script>window.location.href='https://katedralbandung.org/';</script>";
                } else {
					//send error alert
                    echo '<script>alert("Kode Pembatalan Tidak Terdaftar");</script>';
                }
            } else {
                //send error alert
                echo '<script>alert("Seluruh data wajib diisi");</script>';
            }
        }
        //end form submited
        ?>
    </head>
    <body class="bg-light">
        <div class="container">
            <?php
            if (!empty($config['image'])) {
                echo '<img src="' . WEB_SERVER . '/' . $config['image'] . '" style="max-width:100%;" />';
            }
            ?>
            <div class="py-5 text-center">
                <h2>Form Pembatalan Misa</h2>
				Apabila kode pembatalan hilang dan file telah di-download, kode pembatalan ada pada 5 digit terakhir nama file. <br />
				Cth: <b>Rabu-20-Oktober-2021---1700-WIB-20211019173828-b1967.png</b>. Maka Kode pembatalannya adalah <b>b1967</b>.
<br />
				Untuk pendaftaran misa, silahkan <a href="index.php" style="font-size:larger;"><b>klik disini</b></a>. <br />
                    Terima kasih.</p>
            </div>

            <div class="row">
                <div class="col-md-12 order-md-1">
                    <form class="needs-validation" method="post" action="">
                        <input type="hidden" name="token" value="<?php echo $token; ?>" />
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="firstName">Kode Pembatalan:</label>
								<div class="input-group">
									<input type="text" class="form-control" id="kode" value="" required name="kode">
									<div class="input-group-append">
										<button class="btn btn-primary btn-md btn-block" type="submit" name="submit">BATALKAN</button>
									</div>
								</div>
                                <div class="invalid-feedback">
                                    Kode wajib diisi.
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <footer class="my-5 pt-5 text-muted text-center text-small">
                <p class="mb-1">&copy; 2020 <a href="https://www.byxel.net/">Byxel.net</a></p>
                <p style="font-size: x-small;">Sistem ini merupakan freeware dan dapat diminta dengan menghubungi kontak di atas. God bless us.</p>
            </footer>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
    <script src="assets/dist/js/bootstrap.bundle.js"></script>
    <script src="form-validation.js"></script>
</html>