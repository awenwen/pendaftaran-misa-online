<?php
session_start();
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Form pendaftaran misa">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="author" content="Eldwen - Byxel.net">
        <meta name="generator" content="Jekyll v4.0.1">
        <title>Pendaftaran Misa</title>

        <!-- Bootstrap core CSS -->
        <link href="assets/dist/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="form-validation.css" rel="stylesheet">
        <?php
        //koneksi database
        include('config.php');
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
            header("Location: https://www.google.com/");
        }
        //end koneksi database
        //generate token
        $token = bin2hex(random_bytes(64));
        $_SESSION['token'] = $token;
        //end generate token
        //get config for name and capacity
        $getConfig = mysqli_query($conn, "SELECT * FROM configs");
        $config = mysqli_fetch_assoc($getConfig);

        //end get config for name and capacity
        //create function to clean any post data
        function cleanstr($str) {
            $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
            $result = strtoupper($str);
            $result = htmlentities($result);
            $result = mysqli_real_escape_string($conn, $result);
            return $result;
        }

        //end
        //first initiate for data
        $jadwalv = '';
        $nama = '';
        $phone = '';
        $alamat = '';
        $sex = '';
        $age = '';
        $parokiluar = '';
        $lingkunganv = '';
        $keuskupanlain = '';
        //end initiate data
        //event form submited
        if (isset($_POST['submit']) && isset($_SESSION['token'])) {
            //init status
            $status = 0;
            //end init status
			
            //set value for data
            $jadwalv = cleanstr($_POST['jadwal']);
            $nama = cleanstr($_POST['nama']);
            $phone = cleanstr($_POST['phone']);
            $alamat = cleanstr($_POST['alamat']);
            $sex = cleanstr($_POST['sex']);
            $age = cleanstr($_POST['age']);
            $asalparoki = cleanstr($_POST['asalparoki']);
            $parokiluar = cleanstr($_POST['parokiluar']);
            $lingkunganv = cleanstr($_POST['lingkungan']);
            $asalkeuskupan = cleanstr($_POST['asalkeuskupan']);
            $keuskupanlain = cleanstr($_POST['keuskupanlain']);
            $ipaddress = cleanstr($_SERVER['REMOTE_ADDR']);
            $tokenv = $_SESSION['token'];
            $code = hash('sha256', $tokenv . $phone);

            if ($asalkeuskupan != 'KEUSKUPAN BANDUNG') {
                $asalkeuskupan = $keuskupanlain;
            } else {
                $asalkeuskupan = "KEUSKUPAN BANDUNG";
            }
			
			//just random things :)
			//please improved to cURL method for POST method
			$rand1 = substr(md5(uniqid(rand(), true)), 0, 5);
			$rand2 = substr(md5(uniqid(rand(), true)), 0, 5);
			$rand3 = substr(md5(uniqid(rand(), true)), 0, 5);
			
            //end set value
            //double check if js not running
            if (!empty($jadwalv) && !empty($nama) && !empty($phone) && !empty($alamat) && !empty($sex) && !empty($age)) {
                //cek terdaftar atau tidak
                $cekRegistrasi = mysqli_query($conn, "SELECT * FROM checkin c LEFT JOIN jadwal j ON c.jadwal_id = j.jadwal_id WHERE telepon = '" . $phone . "' AND c.jadwal_id = '" . (int) $jadwalv . "'");
                
                if (mysqli_num_rows($cekRegistrasi) > 0) {
                    //send error alert
                    echo '<script>alert("Nomor sudah pernah daftar di jadwal yang sama");</script>';
                } else {
                    //set sql jika dari paroki dalam
                    $sql_intern = "INSERT INTO checkin SET jadwal_id = " . (int) $jadwalv . ", nama='" . $nama . "', telepon='" . $phone . "', alamat='" . $alamat . "', gender='" . $sex . "', usia_id='" . (int) $age . "', asal_paroki='" . $asalparoki . "', asal_lingkungan='" . (int) $lingkunganv . "', token='" . $tokenv . "', code='" . $code . "', cancel_code='" . $rand1 . "', status='ACTIVE', ip_address='" . $ipaddress . "'";

                    //set sql jika dari paroki luar
                    $sql_extern = "INSERT INTO checkin SET jadwal_id = " . (int) $jadwalv . ", nama='" . $nama . "', telepon='" . $phone . "', alamat='" . $alamat . "', gender='" . $sex . "', usia_id='" . (int) $age . "', asal_paroki='" . $asalparoki . "', paroki_lainnya='$parokiluar', asal_keuskupan='" . $asalkeuskupan . "', token='" . $tokenv . "', code='" . $code . "', cancel_code='" . $rand1 . "', status='ACTIVE', ip_address='" . $ipaddress . "'";

                    //conditional untuk paroki dalam/luar
                    if ($asalparoki == "DALAM") {
                        mysqli_query($conn, $sql_intern);
                        $status++;
                    } else if ($asalparoki == "LUAR") {
                        mysqli_query($conn, $sql_extern);
                        $status++;
                    } else {
                        header("Location: https://www.hukumonline.com/klinik/detail/ulasan/lt525aa19bc487b/ancaman-pidana-bagi-peretas-hacker-akun-facebook-orang-lain/");
                    }

                    if ($status > 0) {
                        //calculate apabila kuota habis, update jadwal tersebut untuk habis (should be improved)
                        $getQuota = mysqli_query($conn, "SELECT COUNT(checkin_id) as total FROM checkin WHERE jadwal_id = '" . (int) $jadwalv . "'");
                        $quota = mysqli_fetch_assoc($getQuota);
                        if ($quota['total'] >= $config['kapasitas']) {
                            //set ke 0 untuk menandakan kuota habis dan tidak muncul
                            mysqli_query($conn, "UPDATE jadwal SET status = '0' WHERE jadwal_id = '" . (int) $jadwalv . "'");
                        }
                        //end
                    }

                    //redirect page, create qr code, using GET method
                    header("Location: success.php?b=" . $rand1 . "&y=" . $rand2 . "&x=" . $code . "&l=" . $rand3 . "");
                }
            } else {
                //send error alert
                echo '<script>alert("Seluruh data wajib diisi");</script>';
            }
        }
        //end form submited
        ?>
    </head>
    <body class="bg-light">
        <div class="container">
            <?php
            if (!empty($config['image'])) {
                echo '<img src="' . WEB_SERVER . '/' . $config['image'] . '" style="max-width:100%;" />';
            }
            ?>
            <div class="py-5 text-center">
                <h2>Form Pendaftaran Misa</h2>
                <p class="lead">Selamat datang di <strong><?php echo $config['nama']; ?></strong>. <br />
                <p style="font-size: small;">Sebelum mengikuti misa, seluruh umat diwajibkan untuk mengisi segala bentuk informasi yang dibutuhkan secara <strong>JUJUR dan AKURAT</strong>. Seluruh informasi dalam proses pendaftaran akan menjadi database yang dimiliki dan dikelola oleh pihak gereja. <br />
				Untuk pembatalan pendaftaran misa apabila sudah terdaftar, silahkan <a href="cancel.php" style="font-size:larger;"><b>klik disini</b></a>. <br />
                    Terima kasih.</p>
            </div>

            <div class="row">
                <div class="col-md-12 order-md-1">
                    <h4 class="mb-3">Data Diri</h4>
                    <form class="needs-validation" method="post" action="">
                        <input type="hidden" name="token" value="<?php echo $token; ?>" />
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="country">Jadwal Misa (Bila tidak terdapat pilihan maka kuota telah habis)</label>
                                <select class="custom-select d-block w-100" id="jadwal" required name="jadwal">
                                    <option value="">Silahkan Pilih</option>
                                    <?php
//get jadwal yg masih ada slot dan hanya di minggu tersebut
                                    $getJadwal = mysqli_query($conn, "SELECT * FROM jadwal WHERE status = 1 AND YEARWEEK(`tanggal`, 1) = YEARWEEK(CURDATE(), 1) ORDER BY tanggal ASC");
                                    while ($jadwal = mysqli_fetch_array($getJadwal, MYSQLI_ASSOC)) {
                                        echo "<option value='" . $jadwal['jadwal_id'] . "'";
                                        if ($jadwalv == $jadwal['jadwal_id']) {
                                            echo ' selected ';
                                        }
                                        echo ">" . $jadwal['nama'] . "</option>";
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Silahkan pilih jadwal yang tersedia.
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="firstName">Nama Lengkap (sesuai KTP)</label>
                                <input type="text" class="form-control" id="name" value="<?php echo $nama; ?>" required name="nama">
                                <div class="invalid-feedback">
                                    Nama wajib diisi.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="lastName">No. Handphone</label>
                                <input type="text" class="form-control" id="phone" value="<?php echo $phone; ?>" required name="phone">
                                <div class="invalid-feedback">
                                    No. HP wajib diisi.
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <label for="alamat">Alamat Lengkap (Sesuai domisili saat ini)</label>
                                <textarea class="form-control" id="alamat" required name="alamat"><?php echo $alamat; ?></textarea>
                                <div class="invalid-feedback">
                                    Alamat wajib diisi.
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="sex">Jenis Kelamin</label>
                                <select class="custom-select d-block w-100" id="sex" required name="sex">
                                    <option value="">Silahkan Pilih</option>
                                    <option value="M" <?php if ($sex == 'M') echo ' selected '; ?>>Laki-Laki</option>
                                    <option value="F" <?php if ($sex == 'F') echo ' selected '; ?>>Perempuan</option>
                                </select>
                                <div class="invalid-feedback">
                                    Jenis Kelamin wajib diisi.
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="age">Usia</label>
                                <select class="custom-select d-block w-100" id="age" required name="age">
                                    <option value="">Silahkan Pilih</option>
                                    <?php
//get data master range usia
                                    $getUsia = mysqli_query($conn, "SELECT * FROM usia WHERE status = 1");
                                    while ($usia = mysqli_fetch_array($getUsia, MYSQLI_ASSOC)) {
                                        echo "<option value='" . $usia['usia_id'] . "'";
                                        if ($age == $usia['usia_id']) {
                                            echo ' selected ';
                                        }
                                        echo ">" . $usia['nama'] . "</option>";
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Usia wajib diisi.
                                </div>
                            </div>
                        </div>

                        <hr class="mb-4">
                        <h4 class="mb-3">Data Paroki</h4>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="asalparoki">Asal Paroki</label>
                                <div class="custom-control custom-radio">
                                    <input id="parokidalam" name="asalparoki" type="radio" class="custom-control-input" value="DALAM" checked required onclick='changeField(this);'>
                                    <label class="custom-control-label" for="parokidalam">Paroki <?php echo $config['nama']; ?></label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input id="parokiluar" name="asalparoki" type="radio" class="custom-control-input" value="LUAR" onclick='changeField(this);'>
                                    <label class="custom-control-label" for="parokiluar">Lainnya</label>
                                    <input type="text" class="form-control" placeholder="" value="<?php echo $parokiluar; ?>" name="parokiluar" onclick="getElementById('parokiluar').checked = true; changeField(getElementById('parokiluar'));">
                                </div>
                            </div>
                            <div class="col-md-6 mb-3" id="paroki1">
                                <label for="lingkungan">Asal Lingkungan *</label>
                                <select class="custom-select d-block w-100" id="lingkungan" name="lingkungan">
                                    <option value="">Pilih Asal Lingkungan</option>
                                    <?php
//get data lingkungan yang terdaftar
                                    $getLingkungan = mysqli_query($conn, "SELECT * FROM lingkungan WHERE status = 1 ORDER BY nama");
                                    while ($lingkungan = mysqli_fetch_array($getLingkungan, MYSQLI_ASSOC)) {
                                        echo "<option value='" . $lingkungan['lingkungan_id'] . "'";
                                        if ($lingkunganv == $lingkungan['lingkungan_id'])
                                            echo ' selected ';
                                        echo ">" . $lingkungan['nama'] . "</option>";
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Silahkan pilih lingkungan yang tersedia.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3" id="paroki2" style="display:none;">
                                <label for="asalkeuskupan">Asal Keuskupan</label>
                                <div class="custom-control custom-radio">
                                    <input id="keuskupandalam" name="asalkeuskupan" type="radio" class="custom-control-input" checked required value="KEUSKUPAN BANDUNG">
                                    <label class="custom-control-label" for="keuskupandalam">Keuskupan Bandung</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input id="keuskupanluar" name="asalkeuskupan" type="radio" class="custom-control-input">
                                    <label class="custom-control-label" for="keuskupanluar">Lainnya</label>
                                    <input type="text" class="form-control" id="keuskupanluar" placeholder="" value="<?php echo $keuskupanlain; ?>" name="keuskupanlain" onclick="getElementById('keuskupanluar').checked = true;">
                                </div>
                            </div>
                        </div>

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="agree" required name="agree">
                            <label class="custom-control-label" for="agree">Saya akan mematuhi seluruh protokol "new normal" / Adaptasi Kebiasaan Baru selama mengikuti misa di <?php echo $config['nama']; ?>. *</label>
                        </div>
                        <hr class="mb-4">
                        <button class="btn btn-primary btn-lg btn-block" type="submit" name="submit">DAFTAR</button>
                    </form>
                </div>
            </div>

            <footer class="my-5 pt-5 text-muted text-center text-small">
                <p class="mb-1">&copy; 2020 <a href="https://www.byxel.net/">Byxel.net</a></p>
                <p style="font-size: x-small;">Sistem ini merupakan freeware dan dapat diminta dengan menghubungi kontak di atas. God bless us.</p>
            </footer>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
    <script src="assets/dist/js/bootstrap.bundle.js"></script>
    <script src="form-validation.js"></script>
    <script>
                                        function changeField(el) {
                                            if (el.value == 'DALAM') {
                                                $('#paroki1').css('display', 'block');
                                                $('#paroki2').css('display', 'none');
                                            } else if (el.value == 'LUAR') {
                                                $('#paroki2').css('display', 'block');
                                                $('#paroki1').css('display', 'none');
                                            }
                                        }
    </script>
</html>